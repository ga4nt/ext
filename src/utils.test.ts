import { Fridge, ReferenceCounter, Repeater } from "./utils";

describe("Repeater", () => {
	it("repeats itself", () => {
		const resource = [1, 2, 3];
		let expected = [...resource, ...resource];
		const iterator = new Repeater(resource);

		const actual = [];
		for (let i = 0; i < resource.length * 2; i++)
			actual.push(iterator.next().value);

		expect(actual).toEqual(expected);
	});

	it("never ends", () => {
		const resource = [1, 2, 3];
		const iterator = new Repeater(resource);

		const actual = [];
		for (let i = 0; i < resource.length * 2; i++)
			actual.push(iterator.next().done);

		actual.forEach((done) => expect(done).toBe(false));
	});
});

describe("Fridge", () => {
	const makePromises = (expected: number[]): Promise<number>[] => {
		const makePromise = (n: number) => {
			return new Promise<number>((resolve) => resolve(n));
		};
		return expected.map((v) => makePromise(v));
	};

	const harvest = <T>(fridge: Fridge<T>, n: number): T[] => {
		const actual: T[] = [];
		for (let i = 0; i < n; i++)
			setImmediate(() => actual.push(fridge.next().value));
		return actual;
	};

	it("populates on construction", () => {
		const expected = [1, 2, 3];
		const promises = makePromises(expected);
		const promisesIter = promises[Symbol.iterator]();

		return Promise.all(promises).then(() => {
			const fridge = new Fridge<number>(
				() =>
					promisesIter.next().value ??
					new Promise((_, reject) => reject()).catch(() => {}),
			);
			const actual = harvest(fridge, expected.length);

			setImmediate(() => expect(actual).toEqual(expected));
		});
	});

	it("is sequential", () => {
		const expected = [1, 2, 3];
		const promises = makePromises([...expected, 4, 5, 6]);
		const promisesIter = promises[Symbol.iterator]();

		return Promise.all(promises).then(() => {
			const fridge = new Fridge<number>(() => promisesIter.next().value);
			const actual = harvest(fridge, expected.length);

			setImmediate(() => expect(actual).toEqual(expected));
		});
	});

	it("cycles if new values aren't provided", () => {
		const initialValues = [1, 2, 3];
		const expected = [...initialValues, ...initialValues, ...initialValues];
		const fridge = new Fridge<unknown>(
			() => new Promise((_, reject) => reject()).catch(() => {}),
			initialValues,
		);

		const actual = harvest(fridge, expected.length);

		setImmediate(() => expect(actual).toEqual(expected));
	});

	it("only cycles through last provided values", () => {
		const values = [1, 2, 3, 4, 5, 6];
		const promises = makePromises(values);
		const promisesIter = promises[Symbol.iterator]();
		const fridge = new Fridge(
			() =>
				promisesIter.next().value ??
				new Promise((_, reject) => reject()).catch(() => {}),
			[],
			3,
		);

		const firstRound = harvest(fridge, 3);
		setImmediate(() => expect(firstRound).toEqual(values.slice(0, 3)));

		const secondRound = harvest(fridge, 3);
		setImmediate(() => expect(secondRound).toEqual(values.slice(3, 6)));

		const thirdRound = harvest(fridge, 3);
		setImmediate(() => expect(thirdRound).toEqual(values.slice(3, 6)));
	});

	it("puts used values in the trash", () => {
		const values = [1, 2, 3, 4, 5, 6];
		const promises = makePromises(values);
		const promisesIter = promises[Symbol.iterator]();
		const trash: number[] = [];
		const fridge = new Fridge(
			() =>
				promisesIter.next().value ??
				new Promise((_, reject) => reject()).catch(() => {}),
			[],
			3,
			trash,
		);

		const expected = harvest(fridge, 3);

		setImmediate(() => expect(trash).toEqual(expected));
	});
});

describe("ReferenceCounter", () => {
	it("can put and delete an item with a single subscriber", () => {
		const item = 1;
		const subscriber = "subscriber";
		const trash: number[] = [];
		const rc = new ReferenceCounter(trash);

		rc.subscribe(item, subscriber);
		rc.unsubscribe(subscriber);

		expect(trash).toEqual([item]);
	});

	it("can put and delete an item with multiple subscribers", () => {
		const item = 1;
		const subscribers = ["subscriber1", "subscriber2"];
		const trash: number[] = [];
		const rc = new ReferenceCounter(trash);

		for (const subscriber of subscribers) rc.subscribe(item, subscriber);

		rc.unsubscribe(subscribers[0]);
		expect(trash).toEqual([]);
		rc.unsubscribe(subscribers[1]);
		expect(trash).toEqual([item]);
	});

	it("can put and delete multiple items with a single subscriber", () => {
		const items = [1, 2];
		const subscriber = "subscriber";
		const trash: number[] = [];
		const rc = new ReferenceCounter(trash);

		for (const item of items) rc.subscribe(item, subscriber);

		rc.unsubscribe(subscriber);
		expect(trash).toEqual([1, 2]);
	});

	it("can put and delete multiple items with multiple subscribers", () => {
		const items = [1, 2];
		const subscribers = [
			"subscriber1",
			"subscriber2",
			"subscriber3",
			"subscriber4",
		];
		const trash: number[] = [];
		const rc = new ReferenceCounter(trash);

		for (const subscriber of subscribers.slice(0, 2))
			rc.subscribe(items[0], subscriber);
		for (const subscriber of subscribers.slice(2, 4))
			rc.subscribe(items[1], subscriber);

		rc.unsubscribe(subscribers[0]);
		expect(trash).toEqual([]);
		rc.unsubscribe(subscribers[1]);
		expect(trash).toEqual([1]);
		rc.unsubscribe(subscribers[2]);
		expect(trash).toEqual([1]);
		rc.unsubscribe(subscribers[3]);
		expect(trash).toEqual([1, 2]);
	});
});
