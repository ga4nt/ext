import browser from "webextension-polyfill";
import { RandomImage, FetchedImage } from "./images";
import { Fridge, ReferenceCounter, Repeater } from "./utils";

const REMOTE_HOST = process.env.REMOTE_HOST;
const PORTS: browser.Runtime.Port[] = [];

export interface Source {
	title: string;
	amount: number;
}

const imagesToFetchIter = fetch(`${REMOTE_HOST}/listing.json`)
	.then((resp) => resp.json())
	.then((sources: Source[]) => {
		return new Repeater(new RandomImage(sources));
	});

const unqueued: FetchedImage[] = [];
const fetchedImages = new Fridge<FetchedImage>(
	async () => {
		const iter = await imagesToFetchIter;
		const meta: FetchedImage = iter.next().value;
		return fetch(meta.imageHref)
			.then((res) => res.blob())
			.then((blob) => URL.createObjectURL(blob))
			.then((objectURL) => {
				return { ...meta, objectURL } as FetchedImage;
			})
			.catch(() => {});
	},
	[],
	3,
	unqueued,
);

const unused: FetchedImage[] = [];
const rc = new ReferenceCounter(unused);

const cleanup = (id: number) => {
	rc.unsubscribe(id);
	for (let i = 0; i < unused.length; i++) {
		for (let j = 0; j < unqueued.length; j++) {
			if (unused[i] === unqueued[j]) {
				URL.revokeObjectURL(unused[i].objectURL);
				unused.splice(i, 1);
				unqueued.splice(j, 1);
			}
		}
	}
};

browser.tabs.onRemoved.addListener(cleanup);
browser.tabs.onUpdated.addListener(cleanup);

browser.runtime.onConnect.addListener((port: browser.Runtime.Port) => {
	port.onMessage.addListener(() => {
		const image = fetchedImages.next().value;
		rc.subscribe(image, port.sender!.tab!.id);
		port.postMessage(image);
	});
	PORTS.push(port);
});
