import { useState, useCallback, useMemo, useEffect } from "preact/hooks";
import { FetchedImage } from "../images";
import * as styles from "./Background.module.scss";
import BackgroundRcm from "./BackgroundRcm";
import RcmPortal from "./RcmPortal";

export default function Background({ img }: {
	img?: FetchedImage
}) {
	const url = useMemo(() => {
		return (img) ? `url(${img.objectURL})` : undefined;
	}, [img]);

	const [pos, setPos] = useState<{
		pageX: number,
		pageY: number
	}>();
	const onContextMenu = useCallback((event: MouseEvent) => {
		event.preventDefault();
		if (pos) setPos(undefined);
		else setPos({pageX: event.pageX, pageY: event.pageY});
	}, []);
	const onClick = useCallback((event: MouseEvent) => {
		event.preventDefault();
		setPos(undefined);
	}, []);
	const closeRcm = useCallback(() => {
		setPos(undefined);
	}, []);

	return (<>
		<div style={{
			backgroundImage: url
		}} class={styles.bg} />
		{/* rome-ignore lint/a11y/useKeyWithClickEvents: no need for keyboard alternative */}
		<div
			style={{
				background: img ? "transparent" : undefined
			}}
			class={styles.cover}
			onClick={onClick}
			onContextMenu={onContextMenu}
		/>
		{img
			?
			<RcmPortal cursorPos={pos}>
				<BackgroundRcm img={img} closeRcm={closeRcm} />
			</RcmPortal>
			: undefined}
	</>);
}
