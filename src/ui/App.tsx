import browser from "webextension-polyfill";
import { render } from "preact";
import { useEffect, useState } from "preact/hooks";
import * as styles from "./App.scss";
import Background from "./Background";
import { ImageMeta } from "../types";

function App() {
	const [port] = useState(() => {
		return browser.runtime.connect();
	});

	const [img, setImg] = useState<ImageMeta>();

	useEffect(() => {
		port.onMessage.addListener((msg: ImageMeta) => {
			setImg(msg);
		});
		port.postMessage({});
	}, [port]);

	return (<div style={styles}>
		<Background img={img} />
	</div>);
}

render(<App />, document.getElementById("app")!);
