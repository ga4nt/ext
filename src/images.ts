import { Source } from "./background-script";
import { RandomFill } from "./random";
import { Repeater } from "./utils";

const UPSTREAM_HOST = process.env.UPSTREAM_HOST;
const REMOTE_HOST = process.env.REMOTE_HOST;

export interface ImageMeta {
	title: string;
	filename: string;
	imageHref: string;
	upstreamHref: string;
}

export interface FetchedImage extends ImageMeta {
	objectURL: string;
}

export class RandomImage implements Iterable<ImageMeta> {
	private sources: Source[];

	constructor(sources: Source[]) {
		this.sources = sources;
	}

	[Symbol.iterator](): Iterator<ImageMeta> {
		return new RandomImageIter(this.sources);
	}
}

class RandomImageIter implements Iterator<ImageMeta> {
	private randomSources: RandomImageOfSourceIter[];
	private randomTitlesIter!: Iterator<number>;

	constructor(sources: Source[]) {
		this.randomSources = sources.map((src) => {
			return new RandomImageOfSourceIter(src.title, src.amount);
		});
		this.setRandomTitlesIter();
	}

	private setRandomTitlesIter() {
		this.randomTitlesIter = new Repeater(
			new RandomFill(0, this.randomSources.length - 1),
		);
	}

	next(): IteratorResult<ImageMeta> {
		if (this.randomSources.length === 0) return { done: true, value: null };

		const titleIndex = this.randomTitlesIter.next().value;

		const { done, value } = this.randomSources[titleIndex].next();
		if (done) {
			this.randomSources.splice(titleIndex, 1);
			if (this.randomSources.length === 0) return { done: true, value: null };
			this.setRandomTitlesIter();
			return this.next();
		}

		return {
			done: false,
			value,
		};
	}
}

export class RandomImageOfSourceIter implements Iterator<ImageMeta> {
	title: string;
	numbers: Iterator<number>;

	constructor(title: string, amount: number) {
		this.title = title;
		this.numbers = new RandomFill(1, amount);
	}

	next(): IteratorResult<ImageMeta> {
		const { done, value: n } = this.numbers.next();
		if (done) return { done: true, value: null };
		const filename = `${this.title}${String(n).padStart(3, "0")}.jpg`;
		return {
			done: false,
			value: {
				title: this.title,
				filename,
				imageHref: `${REMOTE_HOST}/${filename.replaceAll("-", "")}`,
				upstreamHref: `${UPSTREAM_HOST}/${this.title}/#frame&gid=1&pid=${n}`,
			} as ImageMeta,
		};
	}
}
